#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int columnas, filas;
int columnasBusqueda, filasBusqueda;
int contador = 0;

int **abrirArchivo(char *nombreArchivo, int *columnas, int *filas) {
    int i, j;

    FILE *archivoMatriz = fopen(nombreArchivo, "r");

    if (archivoMatriz == NULL) {
        return NULL;
    }

    fscanf(archivoMatriz, "%d", columnas);
    fscanf(archivoMatriz, "%d", filas);

    int **matriz = (int **) malloc(sizeof(int *) * *filas);
    for (i = 0; i < *filas; i++) {
        matriz[i] = (int *) malloc(sizeof(int) * *columnas);
    }

    for (i = 0; i < *filas; i++) {
        for (j = 0; j < *columnas; j++) {
            fscanf(archivoMatriz, "%d", &(matriz[i][j]));
        }
    }
    fclose(archivoMatriz);

    return matriz;
}

int buscarFoto(int filaEntrada, int columnaEntrada, int **matrizDondeBuscar, int **matrizFoto) {
    int columnaInicial = columnaEntrada;
    int contadorFila= 0;
    bool esEncontrado = true;
    int contadorColumna = 0;

    while (contadorFila < filasBusqueda ) {
        contadorColumna = 0;
        while (contadorColumna < columnasBusqueda && esEncontrado == true) {
            if (matrizDondeBuscar[filaEntrada + contadorFila][columnaEntrada + contadorColumna] !=
                matrizFoto[contadorFila][contadorColumna]) {
                esEncontrado = false;
            }
            contadorColumna ++;
        }
        contadorFila  ++;
    }
    if (esEncontrado) {
        return 1;
    } else {
        return 0;
    }


}
int limpiezaDeRecursos(int **matriz, int **matrizBusqueda) {
    for (int i = 0; i < filas; ++i) {
        free(matriz[i]);
    }
    free(matriz);

    for (int i = 0; i < filas; ++i) {
        free(matrizBusqueda[i]);
    }
    free(matrizBusqueda);

    return 0;
}

void limpiarMatriz(int **matriz){
    for (int i = 0; i < filas; ++i) {
        free(matriz[i]);
    }
    free(matriz);

}

int main(void) {

    int **matriz = abrirArchivo("entrada.in", &columnas, &filas);
    if (matriz == NULL) {
        return -1;
    }
    int **matrizBusqueda = abrirArchivo("buscar.in", &columnasBusqueda, &filasBusqueda);
    if (matrizBusqueda == NULL) {
        return -2;
    }
    for (int i = 0; i < filas; ++i) {
        for (int j = 0; j < columnas; ++j) {
            // Consulto si el valor de la matriz de entrada es igual al primer valor de la matriz de la matriz buscar y además valido que la fila y columna donde se encuentre
            // este dento del rango posible
            if (matriz[i][j] == matrizBusqueda[0][0] && (filas - i > 0) && (columnas - j > 0)) {
                contador = contador + buscarFoto(i, j, matriz, matrizBusqueda);
            }
        }
    }

    printf("La foto ha sido encontrada: %d veces\n", contador);
    limpiarMatriz(matriz);
    printf("El programa finalizo correctamente\n");

    return 0;
}